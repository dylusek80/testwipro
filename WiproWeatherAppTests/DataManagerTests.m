//
//  DataManagerTests.m
//  WiproWeatherApp
//
//  Created by Michal on 06/09/2016.
//  Copyright © 2016 XPE Mobile. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataManager.h"

@interface DataManagerTests : XCTestCase

@end


@interface DataManager (TestHelpers)

- (NSURL *)getOpenWeatherURL;

@end

@interface DataManagerMoc: DataManager

@property (nonatomic, assign) BOOL hasGoodData;

- (NSURL *)getOpenWeatherURL;

@end

@implementation DataManagerMoc

- (NSURL *)getOpenWeatherURL {
    
    if (!_hasGoodData) {
        return [NSURL fileURLWithPath:[[NSBundle bundleForClass:[DataManagerTests class]] pathForResource:@"OpenWeatherApp_bad_data" ofType:@"json"]];
    }
    
    return [super getOpenWeatherURL];
}

@end

@implementation DataManagerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDataManager {
    
    DataManager *dataManager = [DataManager sharedInstance];
    
    XCTAssertNotNil(dataManager);
}

- (void)testDataManagerCorruptedData {
    
    XCTestExpectation *getCorruptedDataExpectation = [self expectationWithDescription:@"Got a corrupted data"];
    
    DataManagerMoc *dataManager = [DataManagerMoc new];
    
    dataManager.hasGoodData = NO;
    
    [dataManager getForecastDataWithCompletionHandler:^(WeatherForecast *forecast, NSError* error) {
        
        XCTAssertNotNil(error);
        XCTAssertNil(forecast);
        
        [getCorruptedDataExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:2 handler:^(NSError * _Nullable error) {}];
}

-(void)testDataManagerGoodData {
    
    XCTestExpectation *getGoodDataExpectation = [self expectationWithDescription:@"Got a good data"];
    
    DataManagerMoc *dataManager = [DataManagerMoc new];
    dataManager.hasGoodData = YES;
    
   [dataManager getForecastDataWithCompletionHandler:^(WeatherForecast *forecast, NSError* error) {
        
        XCTAssertNil(error);
        XCTAssertNotNil(forecast);
        
        [getGoodDataExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:2 handler:^(NSError * _Nullable error) {}];
}

@end
