//
//  ForecastTests.m
//  WiproWeatherApp
//

#import <XCTest/XCTest.h>
#import "ForecastDetails.h"

@interface ForecastTests : XCTestCase

@end

@implementation ForecastTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testForecastDetails {
    
    ForecastDetails *forecastDetails = [[ForecastDetails alloc] initWithDictionary:nil];
    
    XCTAssertNotNil(forecastDetails);
}

- (void)testForcastDetailsSetup {
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[ForecastTests class]] pathForResource:@"ForecastDetails" ofType:@"plist"]]];
    
    ForecastDetails *forecastDetails = [[ForecastDetails alloc] initWithDictionary:dictionary];
    
    XCTAssertEqual([forecastDetails.timeOfData longValue], 1473163200);
    XCTAssertEqual([forecastDetails.temperature doubleValue], 23.36);
    XCTAssertEqual([forecastDetails.clouds longValue], 88);
    XCTAssertEqual([forecastDetails.wind doubleValue], 1.97);
}

@end
