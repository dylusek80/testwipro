//
//  WeatherForecast.m
//  WiproWeatherApp
//

#import <XCTest/XCTest.h>
#import "WeatherForecast.h"
#import "ForecastDetails.h"

@interface WeatherForecast (TestHelpers)

@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, strong) NSNumber *numberOfListedForecasts;
@property (nonatomic, strong) NSMutableArray *arrayOfDays;

@end

@interface WeatherForecastTests : XCTestCase

@end

@implementation WeatherForecastTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testWeatherForecast {
    
    WeatherForecast *weatherForecast = [[WeatherForecast alloc] initWithDictionary:nil];
    
    XCTAssertNotNil(weatherForecast);
}

- (void)testWeatherForecastSetup {
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[WeatherForecastTests class]] pathForResource:@"WeatherForecast" ofType:@"plist"]]];
    
    WeatherForecast *weatherForecast = [[WeatherForecast alloc] initWithDictionary:dictionary];
    
    XCTAssertTrue([weatherForecast.cityName isEqualToString:@"London"]);
    XCTAssertEqual([weatherForecast.numberOfListedForecasts integerValue], 40);
    XCTAssertEqual(weatherForecast.arrayOfDays.count, 6);
}

- (void)testWeatherForecastNumberOfDays {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[WeatherForecastTests class]] pathForResource:@"WeatherForecast" ofType:@"plist"]]];
    
    WeatherForecast *weatherForecast = [[WeatherForecast alloc] initWithDictionary:dictionary];
    
    XCTAssertEqual([weatherForecast numberOfDays], 6);
}

- (void)testWeatherForecastNumberOfItemsPerDayAtIndex {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[WeatherForecastTests class]] pathForResource:@"WeatherForecast" ofType:@"plist"]]];
    
    WeatherForecast *weatherForecast = [[WeatherForecast alloc] initWithDictionary:dictionary];
    XCTAssertEqual([weatherForecast numberOfItemsPerDayAtIndex:0], 3);
    XCTAssertEqual([weatherForecast numberOfItemsPerDayAtIndex:1], 8);
    XCTAssertEqual([weatherForecast numberOfItemsPerDayAtIndex:5], 5);
    
}

- (void)testWeatherForecastNameForDayAtSection {
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[WeatherForecastTests class]] pathForResource:@"WeatherForecast" ofType:@"plist"]]];
    
    WeatherForecast *weatherForecast = [[WeatherForecast alloc] initWithDictionary:dictionary];
    XCTAssertTrue([[weatherForecast nameForDayAtSection:0] isEqualToString:@"Tuesday"]);
    XCTAssertTrue([[weatherForecast nameForDayAtSection:1] isEqualToString:@"Wednesday"]);
    XCTAssertTrue([[weatherForecast nameForDayAtSection:5] isEqualToString: @"Sunday"]);
}

- (void)testWeatherForecastForecastDetailsAtIndexPath {
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[WeatherForecastTests class]] pathForResource:@"WeatherForecast" ofType:@"plist"]]];
    
    WeatherForecast *weatherForecast = [[WeatherForecast alloc] initWithDictionary:dictionary];
    
    ForecastDetails * forecastDetails = [weatherForecast forecastDetailsAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    XCTAssertNotNil(forecastDetails);
}

@end
