//
//  WiproWeatherAppTests.m
//  WiproWeatherAppTests
//

#import <XCTest/XCTest.h>
#import "NetworkServices.h"

@interface NetworkServices (TestHelpers)

- (NSURLSession *)getURLSession;

@end

@interface NetworkServicesTests : XCTestCase

@end

@implementation NetworkServicesTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNetworkServices {
    
    NetworkServices *networkServices = [NetworkServices sharedInstance];
    XCTAssertNotNil(networkServices);
}

- (void)testNetworkServicesGetURLSession {
    
    NetworkServices *networkServices = [NetworkServices sharedInstance];
    
    NSURLSession *urlSession = [networkServices getURLSession];
    
    XCTAssertNotNil(urlSession);
    XCTAssertNil(urlSession.configuration.HTTPCookieStorage);
    XCTAssertNil(urlSession.configuration.URLCache);
    XCTAssertNil(urlSession.configuration.URLCredentialStorage);
    XCTAssertEqual(urlSession.configuration.requestCachePolicy, NSURLRequestReloadIgnoringCacheData);
}

- (void)testNetworkWhenWeHaveGoodData {
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle bundleForClass:[NetworkServicesTests class]] pathForResource:@"OpenWeatherApp" ofType:@"json"]];
    
    XCTestExpectation *getGoodDataExpectation = [self expectationWithDescription:@"Got a good data"];
    
    NetworkServices *networkServices = [NetworkServices sharedInstance];
    
    [networkServices getDataForURL:url successfulHandler:^(NSData *data) {
        
        XCTAssertNotNil(data);
        [getGoodDataExpectation fulfill];
        
    } errorHandler:^(NSError *error) {
        
        XCTAssertNil(error);
        [getGoodDataExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:2 handler:^(NSError * _Nullable error) {}];
}

- (void)testNetworkBrokenURL {
    
    NSString * urlString = [[NSBundle bundleForClass:[NetworkServicesTests class]] pathForResource:@"OpenWeatherApp" ofType:@"json"];
    urlString = [urlString stringByAppendingString:@"_broken"];
    
    NSURL *url = [NSURL fileURLWithPath:urlString];
    
    XCTestExpectation *getBrokenURLExpectation = [self expectationWithDescription:@"broken URL"];
    
    NetworkServices *networkServices = [NetworkServices sharedInstance];
    
    [networkServices getDataForURL:url successfulHandler:^(NSData *data) {
        
        XCTAssertNil(data);
        [getBrokenURLExpectation fulfill];
        
    } errorHandler:^(NSError *error) {
        
        XCTAssertNotNil(error);
        [getBrokenURLExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:2 handler:^(NSError * _Nullable error) {}];
}

@end
