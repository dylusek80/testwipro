//
//  NetworkServices.m
//  WiproWeatherApp
//

#import "NetworkServices.h"
#import <UIKit/UIKit.h>

@interface NetworkServices ()

- (NSURLSession *)getURLSession;

@end

@implementation NetworkServices

+ (NetworkServices *)sharedInstance {
    static NetworkServices *networkService;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        networkService = [[NetworkServices alloc] init];
    });
    
    return networkService;
}

- (NSURLSession *)getURLSession {
    NSURLSessionConfiguration *urlSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    urlSessionConfiguration.HTTPCookieStorage = nil;
    urlSessionConfiguration.URLCache = nil;
    urlSessionConfiguration.URLCredentialStorage = nil;
    urlSessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:urlSessionConfiguration];
    
    return urlSession;
}

- (void)getDataForURL:(NSURL *)url successfulHandler:(void(^)(NSData *data))successfulHandler errorHandler:(void(^)(NSError *error))errorHandler {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSession *urlSession = [self getURLSession];
    
    NSURLSessionTask *urlSessionTask = [urlSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error == nil) {
            NSHTTPURLResponse *httpURLResponse = (NSHTTPURLResponse*)response;
            
            if (data != nil && data.bytes > 0) {
                if (httpURLResponse.URL.isFileURL) {
                    successfulHandler(data);
                    
                } else {
                    if (httpURLResponse.statusCode == 200 ) {
                        successfulHandler(data);
                        
                    } else {
                        NSError *error = [NSError errorWithDomain:@"com.test.Wipro" code:-1 userInfo:nil];
                        errorHandler(error);
                    }
                }
            }
            else {
                NSError *error = [NSError errorWithDomain:@"com.test.Wipro" code:-1 userInfo:nil];
                errorHandler(error);
            }
            
        } else {
            errorHandler(error);
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
    
    [urlSessionTask resume];
}

@end

