//
//  NetworkServices.h
//  WiproWeatherApp
//

#import <Foundation/Foundation.h>

@interface NetworkServices : NSObject

+ (NetworkServices *)sharedInstance;

- (void)getDataForURL:(NSURL *)url successfulHandler:(void(^)(NSData *data))successfulHandler errorHandler:(void(^)(NSError *error))errorHandler;



@end
