//
//  ForecastDetails.h
//  WiproWeatherApp
//

#import <Foundation/Foundation.h>

@interface ForecastDetails : NSObject

@property (nonatomic, strong) NSNumber *timeOfData;
@property (nonatomic, strong) NSNumber *temperature;
@property (nonatomic, strong) NSNumber *clouds;
@property (nonatomic, strong) NSNumber *wind;
@property (nonatomic, strong) NSNumber *rain;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
