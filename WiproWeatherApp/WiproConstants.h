//
//  WiproConstants.h
//  WiproWeatherApp
//

#import <Foundation/Foundation.h>


//OpenWeatherApp dictionarykeys
extern NSString * const WiproOpenWeatherAppCityKey;
extern NSString * const WiproOpenWeatherAppNameKey;
extern NSString * const WiproOpenWeatherAppCNTKey;
extern NSString * const WiproOpenWeatherAppListKey;
extern NSString * const WiproOpenWeatherAppDateTimeKey;
extern NSString * const WiproOpenWeatherAppMainKey;
extern NSString * const WiproOpenWeatherAppTempKey;
extern NSString * const WiproOpenWeatherAppCloudsKey;
extern NSString * const WiproOpenWeatherAppAllKey;
extern NSString * const WiproOpenWeatherAppWindKey;
extern NSString * const WiproOpenWeatherAppSpeedKey;
extern NSString * const WiproOpenWeatherAppRainKey;
extern NSString * const WiproOpenWeatherApp3hKey;

//OpenWeatherApp URL keys
extern NSString * const WiproOpenWeatherURLKey;

//
extern NSInteger const WiproOpenWeatherMaxNumberOfForecstsPerDay;
extern NSInteger const WiproOpenWeatherMaxNumberOfDays;
