//
//  ViewController.m
//  WiproWeatherApp
//

#import "ViewController.h"
#import "DataManager.h"
#import "WeatherForecast.h"
#import "ForecastDetails.h"

@interface ViewController ()

@property (nonatomic, retain) WeatherForecast *forecastData;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getForecast];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)getForecast {
    DataManager *dataManager = [DataManager sharedInstance];
    
    [dataManager getForecastDataWithCompletionHandler:^(WeatherForecast *forecast, NSError *error) {
        
        self.forecastData = forecast;
        
        __weak ViewController *weakSelf = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.description preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [weakSelf presentViewController:alert animated:YES completion:nil];
            }
            else {
                [weakSelf.tableView reloadData];
            }
        });
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.forecastData numberOfDays];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.forecastData numberOfItemsPerDayAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *tableCellview = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    ForecastDetails *details = [self.forecastData forecastDetailsAtIndexPath:indexPath];
    [self setupTableViewCell:tableCellview with:details];
    
    return tableCellview;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.forecastData nameForDayAtSection:section];
}

#pragma mark -

- (void)setupTableViewCell:(UITableViewCell *)viewcell with:(ForecastDetails *)forecastDetails {
    viewcell.textLabel.text = [NSString stringWithFormat:@"Temperature: %ldC", [forecastDetails.temperature integerValue]];
    viewcell.detailTextLabel.text = [NSString stringWithFormat:@"Wind: %ldm/s", [forecastDetails.wind integerValue]];
}

@end
