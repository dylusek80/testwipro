//
//  WiproConstants.m
//  WiproWeatherApp
//

#import "WiproConstants.h"

NSString * const WiproOpenWeatherAppCityKey     = @"city";
NSString * const WiproOpenWeatherAppNameKey     = @"name";
NSString * const WiproOpenWeatherAppCNTKey      = @"cnt";
NSString * const WiproOpenWeatherAppListKey     = @"list";
NSString * const WiproOpenWeatherAppDateTimeKey = @"dt";
NSString * const WiproOpenWeatherAppMainKey     = @"main";
NSString * const WiproOpenWeatherAppTempKey     = @"temp";
NSString * const WiproOpenWeatherAppCloudsKey   = @"clouds";
NSString * const WiproOpenWeatherAppAllKey      = @"all";
NSString * const WiproOpenWeatherAppWindKey     = @"wind";
NSString * const WiproOpenWeatherAppSpeedKey    = @"speed";
NSString * const WiproOpenWeatherAppRainKey     = @"rain";
NSString * const WiproOpenWeatherApp3hKey       = @"3h";

NSString * const WiproOpenWeatherURLKey = @"OpenWeatherMapURL";

NSInteger const WiproOpenWeatherMaxNumberOfForecstsPerDay = 8;
NSInteger const WiproOpenWeatherMaxNumberOfDays = 6;