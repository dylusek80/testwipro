//
//  WeatherForecast.h
//  WiproWeatherApp
//

#import <Foundation/Foundation.h>
#import "ForecastDetails.h"

@interface WeatherForecast : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (NSInteger)numberOfDays;
- (NSInteger)numberOfItemsPerDayAtIndex:(NSInteger)index;
- (NSString *)nameForDayAtSection:(NSInteger)section;
- (ForecastDetails *)forecastDetailsAtIndexPath:(NSIndexPath *)indexPath;


@end
