//
//  ForecastDetails.m
//  WiproWeatherApp
//

#import "ForecastDetails.h"
#import "WiproConstants.h"

@interface ForecastDetails ()

@end

@implementation ForecastDetails

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        self.timeOfData = [dictionary objectForKey:WiproOpenWeatherAppDateTimeKey];
        self.temperature = [[dictionary objectForKey:WiproOpenWeatherAppMainKey] objectForKey:WiproOpenWeatherAppTempKey];
        self.clouds = [[dictionary objectForKey:WiproOpenWeatherAppCloudsKey] objectForKey:WiproOpenWeatherAppAllKey];
        self.wind = [[dictionary objectForKey:WiproOpenWeatherAppWindKey] objectForKey:WiproOpenWeatherAppSpeedKey];
        self.rain = [[dictionary objectForKey:WiproOpenWeatherAppRainKey] objectForKey:WiproOpenWeatherApp3hKey];
    }
    return self;
}


@end
