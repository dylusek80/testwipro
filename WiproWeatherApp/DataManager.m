//
//  DataManager.m
//  WiproWeatherApp
//

#import "DataManager.h"
#import "NetworkServices.h"
#import "WiproConstants.h"

@interface DataManager ()

@property (nonatomic, strong) WeatherForecast *weatherForecast;

@end

@implementation DataManager

+ (DataManager *)sharedInstance {
    static DataManager *dataManager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataManager = [[DataManager alloc] init];
    });
    
    return dataManager;
}

- (void)getForecastDataWithCompletionHandler:(void(^)(WeatherForecast *forecast, NSError* error))completionHandler {
    NetworkServices *networkService = [NetworkServices sharedInstance];
    NSURL *url = [self getOpenWeatherURL];
    
    [networkService getDataForURL:url successfulHandler:^(NSData *data) {
        
        NSError *error;
        NSDictionary *feedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            completionHandler(nil, error);
            
        } else {
            self.weatherForecast = [[WeatherForecast alloc] initWithDictionary:feedDictionary];
            completionHandler(self.weatherForecast, error);
        }
    } errorHandler:^(NSError *error) {
        completionHandler(nil, error);
        
    }];
}

- (NSURL *)getOpenWeatherURL {
    
    return [NSURL URLWithString:[[NSBundle mainBundle] objectForInfoDictionaryKey:WiproOpenWeatherURLKey]];
}

@end
