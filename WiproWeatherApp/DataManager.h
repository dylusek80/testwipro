//
//  DataManager.h
//  WiproWeatherApp
//

#import <Foundation/Foundation.h>
#import "WeatherForecast.h"

@interface DataManager : NSObject

+ (DataManager *)sharedInstance;

- (void)getForecastDataWithCompletionHandler:(void(^)(WeatherForecast *forecast, NSError* error))completionHandler;

@end
