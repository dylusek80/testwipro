//
//  WeatherForecast.m
//  WiproWeatherApp
//

#import "WeatherForecast.h"
#import "ForecastDetails.h"
#import "WiproConstants.h"

#import <UIKit/UIKit.h>

@interface WeatherForecast ()

@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, strong) NSNumber *numberOfListedForecasts;
@property (nonatomic, strong) NSMutableArray *arrayOfDays;

@end

@implementation WeatherForecast

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
    
        [self setupWithDictionary:dictionary];
    }
    return self;
}

- (void)setupWithDictionary:(NSDictionary *)dictionary {
    NSDictionary *city = [dictionary objectForKey:WiproOpenWeatherAppCityKey];
    
    self.cityName = [city objectForKey:WiproOpenWeatherAppNameKey];
    self.numberOfListedForecasts = [dictionary objectForKey:WiproOpenWeatherAppCNTKey];
    
    NSArray *forecastArray = [dictionary objectForKey:WiproOpenWeatherAppListKey];
    
    self.arrayOfDays = [NSMutableArray arrayWithCapacity:WiproOpenWeatherMaxNumberOfDays];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger dayTime = [NSDate date].timeIntervalSince1970;
    
    NSMutableArray *arrayOfForecasts = [NSMutableArray arrayWithCapacity:WiproOpenWeatherMaxNumberOfForecstsPerDay];
    
    for (NSDictionary *details in forecastArray) {
        
        ForecastDetails *forecastDetails = [[ForecastDetails alloc] initWithDictionary:details];
        
        if ([calendar isDate:[NSDate dateWithTimeIntervalSince1970:dayTime] inSameDayAsDate:[NSDate dateWithTimeIntervalSince1970:[forecastDetails.timeOfData longValue]]]) {
            [arrayOfForecasts addObject:forecastDetails];
        }
        else {
            dayTime = [forecastDetails.timeOfData longValue];
            [self.arrayOfDays addObject:arrayOfForecasts];
            arrayOfForecasts = [NSMutableArray arrayWithCapacity:WiproOpenWeatherMaxNumberOfForecstsPerDay];
            [arrayOfForecasts addObject:forecastDetails];
        }
    }
    [self.arrayOfDays addObject:arrayOfForecasts];
}


- (NSInteger)numberOfDays {
    return self.arrayOfDays.count;
}

- (NSInteger)numberOfItemsPerDayAtIndex:(NSInteger)index {
    NSArray *arrayOfForecastsForDay = [self.arrayOfDays objectAtIndex:index];
    
    return arrayOfForecastsForDay.count;
}

- (NSString *)nameForDayAtSection:(NSInteger)section {
    NSArray *arrayOfForecastsForDay = [self.arrayOfDays objectAtIndex:section];
    ForecastDetails *details = [arrayOfForecastsForDay firstObject];

    NSTimeInterval timeInterval = [details.timeOfData doubleValue];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:date];
    
    return dayName;
}

- (ForecastDetails *)forecastDetailsAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *arrayOfForecastsForDay = [self.arrayOfDays objectAtIndex:indexPath.section];
    ForecastDetails *details = [arrayOfForecastsForDay objectAtIndex:indexPath.row];
    
    return details;
}

@end
