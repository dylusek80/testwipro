# How to run / build / test
  1. Open WiproWeatherApp.xcodeproj file in Xcode
  2. Choose iPhone 6s iOS 9.3 Simaulator 
  3. Press "Run" or "Test"

# What could be done better
- Improve UI by addeding details view etc
- Add autorefres as app is only loading data on start
- More Unit tests